# JobMatch #
## Proiect Bestem 2017, Accenture ##


*Echipa Expellibugus:*

* Dobroteanu Andreea	331CA
* Moisa Anca-Elena	331CA
* Vatamanu Alexandra	331CA

*Proiectul a fost realizat in folosind urmatoarele tehnologii:*

* **PHP** - framework: *CodeIgniter*
* **Javascript** - framework: *Backbone* (+ alte tool-uri: *jquery*, *bootstrap*)
* **HTML** + **CSS**

Task-urile ce trebuiau realizate sunt cele de pe urmatoarele liste (cele bifate sunt cele implementate):
![Tasks 1](images/quest1.jpg)
![Tasks 2](images/quest2.jpg)


Pentru task-urile realizate, am adaugat urmatoarele print-uri din aplicatia noastra (cu cateva comentarii):

* Landing page - user anonim
![Default Homepage](images/jobmatch1.png)

* Form register admin
![Register Admin](images/jobmatch3.png)

* Form register user (+ campuri cu informatii specifice, spre deosebire de admin)
![Register User](images/job_match.png)

* Userul primeste feedback atunci cand este inregistrat:
![Feedback register](images/jobmatch4.png)

* Form login (acelasi pentru user/admin)
![Login](images/jobmatch2.png)

* Dupa ce user-ul a fost logat, interfata i se va schimba in functie de tipul sau:
* * Admin (poate vedea job-urile pe care le administreaza si se poate deloga):
![Admin Homepage](images/jobmatch5.png)

* * User normal (poate vedea job-urile la care poate aplica, joburile la care a aplicat si se poate deloga):
![User Homepage](images/jobmatch9.png)

* Pagina admin joburi (adminul poate cauta, adauga, edita sau pasa catre alt admin job-urile sale)
![Admin Jobs](images/jobmatch7.png)

* * Form adaugare job admin
![Admin Jobs Add](images/jobmatch6.png)

* * Form pasare job admin
![Admin Jobs Pass](images/jobmatch.png)

* Pagina job-uri la care poate aplica un user (job-uri alese in functie de domeniul ales de user)
![User Jobs Page](images/jobmatch10.png)

* Pagina job-uri la care user-ul a aplicat deja
![User Applications Page](images/jobmatch11.png)

* Un user isi poate uploada CV-ul (implementat in backend, neintegrat in design)
![CV Upload](images/jobmatch12.png)