-- --------------------------------------------------------
-- Server:                       127.0.0.1
-- Versiune server:              5.5.54-0ubuntu0.14.04.1 - (Ubuntu)
-- SO server:                    debian-linux-gnu
-- HeidiSQL Versiune:            9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Descarcă structura pentru tabelă bestem.applications
CREATE TABLE IF NOT EXISTS `applications` (
  `id_user` int(11) NOT NULL,
  `id_job` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Descarcă datele pentru tabela bestem.applications: ~0 rows (aproximativ)
DELETE FROM `applications`;
/*!40000 ALTER TABLE `applications` DISABLE KEYS */;
INSERT INTO `applications` (`id_user`, `id_job`) VALUES
	(28, 2),
	(28, 2),
	(28, 2),
	(32, 2),
	(32, 2),
	(32, 2),
	(32, 2),
	(32, 2),
	(32, 2),
	(32, 2),
	(32, 2),
	(32, 2),
	(32, 2),
	(32, 2),
	(32, 2);
/*!40000 ALTER TABLE `applications` ENABLE KEYS */;

-- Descarcă structura pentru tabelă bestem.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `id_job` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) NOT NULL,
  `candidate_profile` text NOT NULL,
  `name_of_recruiter` varchar(50) NOT NULL,
  `job_description` text NOT NULL,
  `id_domain` varchar(50) NOT NULL,
  `id_admin` int(11) NOT NULL,
  PRIMARY KEY (`id_job`),
  KEY `FK__job_domain` (`id_domain`),
  KEY `FK_jobs_profile` (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Descarcă datele pentru tabela bestem.jobs: ~3 rows (aproximativ)
DELETE FROM `jobs`;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`id_job`, `company_name`, `candidate_profile`, `name_of_recruiter`, `job_description`, `id_domain`, `id_admin`) VALUES
	(1, 'Adobe', '', '', '', 'IT', 12),
	(2, 'Ixia', '', '', '', 'IT', 13),
	(6, 'Google', '', '', '', 'IT', 4),
	(7, 'Microsoft', 'destept', 'a', 'misto', 'IT', 32),
	(8, 'Microsoft', 'destept', 'alex', 'misto', 'IT', 32),
	(9, 'Microsoft', 'destept', 'alex', 'misto', 'IT', 32),
	(10, 'Microsoft', 'destept', 'alex', 'misto', 'IT', 32),
	(11, 'Microsoft', 'destept', 'alex', 'misto', 'IT', 32);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Descarcă structura pentru tabelă bestem.profile
CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(50) NOT NULL DEFAULT '0',
  `first_name` varchar(50) NOT NULL DEFAULT '0',
  `email` varchar(50) NOT NULL DEFAULT '0',
  `pass` varchar(50) NOT NULL DEFAULT '0',
  `studies` text,
  `job_domain` varchar(50) NOT NULL,
  `experience` text,
  `admin` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

-- Descarcă datele pentru tabela bestem.profile: ~16 rows (aproximativ)
DELETE FROM `profile`;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` (`id`, `last_name`, `first_name`, `email`, `pass`, `studies`, `job_domain`, `experience`, `admin`) VALUES
	(13, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', 'Economics', 'meh', 1),
	(14, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', 'IT', 'meh', 1),
	(27, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', 'IT', 'meh', 1),
	(28, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', 'IT', 'meh', 1),
	(29, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', 'IT', 'meh', 1),
	(30, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', 'Economics', 'meh', 1),
	(31, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(32, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(33, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(34, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(35, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(36, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(37, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(38, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(39, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(40, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(41, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(42, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(43, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(44, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(45, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(46, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(47, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(48, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(49, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(50, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(51, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(52, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(53, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(54, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1),
	(55, 'bla', 'bla', 'bla@gmail.com', '128ecf542a35ac5270a87dc740918404', 'bla', '1', 'meh', 1);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;

-- Descarcă structura pentru tabelă bestem.skills
CREATE TABLE IF NOT EXISTS `skills` (
  `id_skills` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_skills`),
  KEY `FK__profile` (`id_user`),
  CONSTRAINT `FK__profile` FOREIGN KEY (`id_user`) REFERENCES `profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Descarcă datele pentru tabela bestem.skills: ~0 rows (aproximativ)
DELETE FROM `skills`;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;

-- Descarcă structura pentru tabelă bestem.test
CREATE TABLE IF NOT EXISTS `test` (
  `col1` varchar(50) DEFAULT NULL,
  `col2` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Descarcă datele pentru tabela bestem.test: ~1 rows (aproximativ)
DELETE FROM `test`;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` (`col1`, `col2`, `id`) VALUES
	('bla', 1, 0);
/*!40000 ALTER TABLE `test` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
