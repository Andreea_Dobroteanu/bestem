<?php

class Unsubscribe extends CI_Controller
{
    public function delete_row()
    {
        $id = $this->input->get('id');
        $this->load->model('Unsubscribe_model');
        $this->Unsubscribe_model->row_delete($id);
    }
}