<?php

class Register extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('register_model');
  }

  public function index()
  {
  }

  public function register_user(){
    $errors = array();

    if(empty($this->input->post('first_name')) || empty($this->input->post('last_name'))
      || empty($this->input->post('email')) || empty($this->input->post('pass')))
    {
      array_push($errors, "Please fill all the fields");
    }

    if (empty($errors)) {
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $email = $this->input->post('email');
        $pass = md5($this->input->post('pass'));
        $studies = $this->input->post('studies');
        $experience = $this->input->post('experience');
        $admin = $this->input->post('admin');
        $job_domain = $this->input->post('job_domain');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            array_push($errors, "Please provide a valid email");
        }
        if (empty($errors)){
            $this->register_model->insert_profile($last_name, $first_name, $email, $pass, $studies, $experience, $admin, $job_domain);
        }
    }

    header('Content-Type: application/json');
    echo json_encode($errors);
  }
}