<?php

class Upload extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        $this->load->view('upload_form', array('error' => ' ' ));
    }

    public function do_upload()
    {
        $this->load->model('Upload_model');

        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'pdf|doc';
        $config['max_size'] = 0;

        $this->load->library('upload', $config);
        $this->upload->do_upload();

        $full_file_path = base_url()."./uploads/".$_FILES['userfile']['name'];
        $this->Upload_model->index($full_file_path);
        echo "CV adaugat cu succes!";
    }
}