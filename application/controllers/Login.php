<?php

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        //load the login model
        $this->load->model('login_model');
    }

    public function index()
    {
        //get the posted values
        $email = $this->input->post("email");
        $password = $this->input->post("pass");

        //check if username and password is correct
        $usr_result = $this->login_model->getUser($email, $password);
        var_dump($usr_result);
        if ($usr_result) {
            //set the session variables
            $session_data = array(
                'id' => $usr_result[0]->id,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($session_data);
        }

        echo json_encode($usr_result);
    }
}