<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->model('form_model');
  }

  public function index()
  {
    $data = $this->form_model->get_last_ten_entries();

    $this->load->view('test_form', $data[0]);
  }
}
