<?php

class Jobs extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('register_model');
    }

    function index() {
        $id_admin = $this->session->userdata('id');

        $data['admin'] = 1;
        $data['data'] = $this->register_model->get_jobs($id_admin);
        $data['all_admins'] = $this->register_model->get_all_admins();

        $this->load->view('jobs_page_view', $data);
    }

    function apply() {
        $id_user = $this->session->userdata('id');

        $res = $this->register_model->apply_job(
            $this->input->post('job_id'),
            $id_user
        );

        return res;
    }

    function add_job() {
        $id_admin = $this->session->userdata('id');

        $res = $this->register_model->add_job(
            $this->input->post('company_name'),
            $this->input->post('candidate_profile'),
            $this->input->post('name_of_recruiter'),
            $this->input->post('job_description'),
            $this->input->post('id_domain'),
            $id_admin
        );

        echo $res;
    }

    function edit_job_company_name() {
        $res = $this->register_model->edit_job(
            $this->input->post('company_name'),
            'company_name',
            $this->input->post('job_id')
        );

        echo $res;
    }

    function edit_job_company_descr() {
        $res = $this->register_model->edit_job(
            $this->input->post('company_name'),
            'job_description',
            $this->input->post('job_id')
        );

        echo $res;
    }

    function leave() {
        $job_id = $this->input->post('job_id');
        $to = $this->input->post('other_admin_id');

        echo $this->register_model->leave_job($job_id, $to);
    }
}