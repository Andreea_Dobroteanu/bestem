<?php

class VerifyLogin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
    }

    function index()
    {
        $this->load->library('form_validation');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login_view');
        }
        else {
            redirect('home', 'refresh');
        }
    }

    function check_database($pass)
    {
        $email = $this->input->post('email');
        $result = $this->user->login($email, $pass);

        if ($result) {
            $sess_array = array();
            foreach ($result as $row) {
                $sess_array = array(
                                    'id' => $row->id,
                                    'email' => $row->email
                                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        }
        else {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }
}