<?php

class Applications extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('register_model');
    }

    function index() {
        $id_user = $this->session->userdata('id');


        $data['admin'] = 0;
        $data['data'] = $this->register_model->get_possible_jobs($id_user);

        $this->load->view('user_jobs', $data);
    }

    function apply_to_job() {
        $id_user = $this->session->userdata('id');

        $res = $this->register_model->apply_to_job(
            $this->input->post('company_name'),
            $this->input->post('candidate_profile'),
            $this->input->post('job_description'),
            $this->input->post('name_of_recruiter'),
            $id_user
        );

        echo $res;
    }
}