<?php

class Home extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->model('profile_model');
    }

    function index()
    {
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $id = $this->session->userdata('id');
            $data = $this->profile_model->get_basic_profile($id);
        } else {
            /* nu e logat */
            $data["admin"] = -1;
        }

        $this->load->view('home_view', $data);
    }

    function logout()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('logged_in');
        session_destroy();
        echo true;
    }
}