<?php

class List_job_applied extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('list_job_applied_model');
    }


    function index() {
        $id_admin = $this->session->userdata('id');
        $data['admin'] = 0;
        $data['data'] = $this->list_job_applied_model->get_applied_jobs($id_admin);
        //$data['all_admins'] = $this->list_job_applied_model->get_all_admins();

        $this->load->view('applications_view', $data);
    }

}