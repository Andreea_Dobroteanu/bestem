<?php

class Register_model  extends CI_Model{

    public function insert_profile($last_name, $first_name, $email, $pass, $studies, $experience, $admin, $job_domain)
    {
        $data = array(
            'id' => '',
            'last_name' => $last_name,
            'first_name' => $first_name,
            'email' => $email,
            'pass' => $pass,
            'studies' => $studies,
            'experience' => $experience,
            'admin' => $admin,
            'job_domain' => $job_domain
        );

        $this->db->insert('profile', $data);
    }

    public function insert_skills($title, $id)
    {
        $data = array(
            'id_skills' => '',
            'title' => $title,
            'id_user' => $id,
        );

        $this->db->insert('skills', $data);
    }

    /* listare job-uri user */
    public function get_jobs($id_admin){
        $this->db->select('*');
        $this->db->from('jobs');
        $this->db->where('id_admin', $id_admin);
        $query = $this->db->get();
        return $query->result();
//        foreach ($query->result() as $row)
//        {
//            var_dump($row->company_name);
//        }
    }

    public function get_possible_jobs($id)
    {
        $this->db->select('job_domain');
        $this->db->from('profile');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->result();
        foreach ($result as $row)
        {
            $this->db->select('*');
            $this->db->from('jobs');
            $this->db->where('id_domain', $row->job_domain);
            $query = $this->db->get();
            $result = $query->result();
        }

        return $result;
    }

    /* aplicare user job */
    public function apply_job($id_job, $id_user){
        $data = array(
            'id_user' => $id_user,
            'id_job' => $id_job
        );

        return $this->db->insert('applications', $data);
    }

    /* listare aplicarile la un job*/
    public function get_apps($id_job){
        $this->db->select('company_name, last_name, first_name, studies, experience');
        $this->db->from('jobs');
        $this->db->join('applications','applications.id_job = jobs.id_job');
        $this->db->join('profile','profile.id = applications.id_user');
        $query = $this->db->get();
        foreach ($query->result() as $row)
        {
            var_dump($row->first_name." ".$row->last_name);
        }
    }

    public function add_job($company_name, $candidate_profile, $name_of_recruiter, $job_description, $id_domain, $id_admin){
        $data = array(
            'id_job' => '',
            'company_name' => $company_name,
            'candidate_profile' => $candidate_profile,
            'name_of_recruiter' => $name_of_recruiter,
            'job_description' => $job_description,
            'id_domain' => $id_domain,
            'id_admin' => $id_admin,
        );
        return $this->db->insert('jobs', $data);
    }

    public function apply_to_job($company_name, $candidate_profile, $job_description, $name_of_recruiter)
    {
        $data = array(
            'company_name' => $company_name,
            'candidate_profile' => $candidate_profile,
            'job_description' => $job_description,
            'name_of_recruiter' => $name_of_recruiter,
        );
        return $this->db->insert('applications', $data);
    }

    public function edit_job($var, $row, $id_job){
        $this->db->set($row, $var);
        $this->db->where('id_job', $id_job);
        return $this->db->update('jobs');
    }

    public function leave_job($id_job, $id_admin){
        $this->db->set('id_admin', $id_admin);
        $this->db->where('id_job', $id_job);
        return $this->db->update('jobs');
    }

    public function get_all_admins() {
        $this->db->select('id, email');
        $this->db->from('profile');
        $this->db->where('admin', 1);
        $query = $this->db->get();
        return $query->result();
    }
}