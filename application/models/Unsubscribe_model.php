<?php

class Unsubscribe_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function row_delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('profile');
    }
}
