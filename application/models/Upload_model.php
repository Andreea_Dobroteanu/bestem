<?php

class Upload_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function index($full_file_path)
    {
        $data = array(
            'cv_path'=>$full_file_path,
        );
        $this->db->insert('profile', $data);
    }
}