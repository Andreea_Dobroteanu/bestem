<?php

class List_job_applied_model extends CI_Model
{

    function get_applied_jobs($id)
    {
        $this->db->select('*');
        $this->db->from('jobs');
        $this->db->join('applications', 'applications.id_job = jobs.id_job');
        $this->db->where('applications.id_user', $id);
        $query = $this->db->get();
        return $query->result();
//        foreach ($query->result() as $row) {
//            var_dump($row->company_name);
//        }
    }

    public function get_all_admins() {
        $this->db->select('id, email');
        $this->db->from('profile');
        $this->db->where('admin', 1);
        $query = $this->db->get();
        return $query->result();
    }
}