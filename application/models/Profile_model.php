<?php

class Profile_model extends CI_Model
{
    public function get_basic_profile($id)
    {
        $this->db->select('id, email, first_name, last_name, admin');
        $this->db->from('profile');
        $this->db->where('id', $id);
        $query = $this->db->get();

        if($query->num_rows() == 1) {
            return $query->result()[0];
        }
        else {
            return false;
        }
    }
}