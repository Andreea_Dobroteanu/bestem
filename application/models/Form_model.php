<?php

class Form_model extends CI_Model{
  public $id;
  public $col1;
  public $col2;

  public function get_last_ten_entries()
  {
    $query = $this->db->get('test', 10);
    return $query->result();
  }

  public function insert_entry()
  {
    $this->title    = $_POST['title']; // please read the below note
    $this->content  = $_POST['content'];
    $this->date     = time();

    $this->db->insert('entries', $this);
  }

  public function update_entry()
  {
    $this->title    = $_POST['title'];
    $this->content  = $_POST['content'];
    $this->date     = time();

    $this->db->update('entries', $this, array('id' => $_POST['id']));
  }

  public function get_news($id) {
    if ($id != FALSE) {
      $this->db->select("*");
      $this->db->from("test");
      $query = $this->db->get();
      return $query->result();
    }
    else {
      return FALSE;
    }
  }
}