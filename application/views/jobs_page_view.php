<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>JobMatch</title>

    <!-- Bootstrap Core CSS -->
    <link href="public/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="public/css/landing-page.css" rel="stylesheet">
    <!-- Custom Fonts -->

    <link href="public/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
    <div class="container topnav">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand topnav" href="/">JobMatch</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <?php
                if ($admin < 0)
                    $this->load->view('partials/default_navbar_buttons');
                else
                    $this->load->view('partials/logged_nav_buttons');
                ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>


<div class="container admin-jobs-container" id="admin_jobs_container">
    <div class="row">
        <div class="col-lg-12">
            <h3>Jobs</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
            <input type="search" id="search" value="" class="form-control" placeholder="Search job...">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table" id="jobs_list_table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Company name</th>
                    <th>Candidate profile</th>
                    <th>Name of recruiter</th>
                    <th>Job description</th>
                    <th>Domain</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $item) { ?>
                    <tr>
                        <td class="job-id-td"><?php echo $item->id_job ?></td>
                        <td class="comp-name">
                            <span><?php echo $item->company_name ?></span>
                            <input type="text" class="comp-name-edit hidden" value="<?php echo $item->company_name?>"/>
                        </td>
                        <td class="cand-profile">
                            <span><?php echo $item->candidate_profile ?></span>
                        </td>
                        <td class="recruiter">
                            <span><?php echo $item->name_of_recruiter ?></span>
                        </td>
                        <td class="job-descr">
                            <span><?php echo $item->job_description ?></span>
                            <input type="text" class="comp-descr-edit hidden" value="<?php echo $item->job_description?>"/>
                        </td>
                        <td class="domain">
                            <span><?php echo $item->id_domain?></span>
                        </td>
                        <td class="delete-job">
                            <a href="#" id="delete_job_button" data-toggle="modal" data-target="#delete_job_modal" class="delete-job-button">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <hr>
        </div>
    </div>
</div>

<a href="#" id="add_job_button" data-toggle="modal" data-target="#add_job_modal" class="add-job-button btn btn-default btn-lg">
    <i class="fa fa-plus"></i>
</a>

<!--<div class="add-job-button">-->
<!--    <i class="fa fa-plus" aria-hidden="true"></i>-->
<!--</div>-->

<div class="modal fade add-job-modal" id="add_job_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="addjobmodal-container">
            <h1>Add a new job</h1><br>
            <form class="add-job-form">
                <div class="form-group">
                    <label for="company-name" class="control-label">Company name:</label>
                    <input type="text" class="form-control company-name">
                </div>
                <div class="form-group">
                    <label class="control-label">Candidate profile:</label>
                    <textarea class="form-control candidate-profile"></textarea>
                </div>

                <div class="form-group">
                    <label class="control-label">Name of recruiter:</label>
                    <input type="text" class="form-control name-of-recruiter">
                </div>

                <div class="form-group">
                    <label class="control-label">Job description:</label>
                    <input type="text" class="form-control job-description">
                </div>
                <div class="form-group">
                    <label for="add-job-domain" class="control-label">Job domain:</label>
                    <select class="form-control domain" id="add-job-domain">
                        <option>IT</option>
                        <option>Marketing</option>
                        <option>Human Resources</option>
                        <option>Medical</option>
                    </select>
                </div>

                <input type="submit" name="add-job" class="add-job addjobmodal-submit" value="Submit">
            </form>
        </div>
    </div>
</div>

<div class="modal fade delete-job-modal" id="delete_job_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="deletejobmodal-container">
            <h2>Are you sure you want to delete this?</h2><br>
            <form class="delete-job-form">

                <div class="form-group">
                    <label for="leave-job-select" class="control-label">Pass it to another admin:</label>
                    <select class="form-control domain" id="leave-job-select">
                        <?php foreach ($all_admins as $other_admin) { ?>
                            <option data-id="<?php echo $other_admin->id ?>"><?php echo $other_admin->email ?></option>
                        <?php } ?>
                    </select>
                </div>

                <input type="submit" name="delete-job" class="delete-job deletejobmodal-submit" value="Yes">
                <input type="button" name="cancel" class="cancel-delete" value="Cancel">
            </form>
        </div>
    </div>
</div>


<div class="alert alert-success">
    <strong></strong>
</div>


<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="copyright copy-container text-muted small">Copyright &copy; Expellibugus</p>
            </div>
        </div>
    </div>
</footer>



<script type='text/javascript' src="public/js/vendor/underscore.min.js"></script>
<script type='text/javascript' src="public/js/vendor/jquery-3.2.1.min.js"></script>
<script type='text/javascript' src="public/js/vendor/backbone.min.js"></script>
<script type='text/javascript' src="public/js/vendor/bootstrap.min.js"></script>
<script src="//rawgithub.com/stidges/jquery-searchable/master/dist/jquery.searchable-1.0.0.min.js"></script>
<script type='text/javascript' src="public/js/views/Landing.js"></script>
<script type='text/javascript' src="public/js/views/Login.js"></script>
<script type='text/javascript' src="public/js/views/Register.js"></script>
<script type='text/javascript' src="public/js/views/AddJobForm.js"></script>
<script type='text/javascript' src="public/js/views/EditJobsList.js"></script>
<script type='text/javascript' src="public/js/views/DeleteForm.js"></script>
<script type='text/javascript' src="public/js/index.js"></script>
</body>
</html>
