<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>JobMatch</title>

	<!-- Bootstrap Core CSS -->
	<link href="public/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="public/css/landing-page.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="public/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
	<div class="container topnav">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand topnav" href="/">JobMatch</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<?php
				if ($admin < 0)
					$this->load->view('partials/default_navbar_buttons');
				else
					$this->load->view('partials/logged_nav_buttons');
				?>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container -->
</nav>


<!-- Header -->
<a name="about"></a>
<div class="intro-header">
	<div class="container" id="landing_container">

		<div class="row">
			<div class="col-lg-12">
				<div class="intro-message">
					<h1>JobMatch</h1>
					<h3>The only app you need for finding the perfect job</h3>

					<hr class="intro-divider">
					<ul class="list-inline intro-social-buttons">
						<?php
						if ($admin < 0)
							$this->load->view('partials/default_home_buttons');
						else
							$this->load->view('partials/logged_home_buttons');
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
<!-- /.intro-header -->
</div>
<!-- /.container -->

<div class="alert alert-success register-success">
	<strong>You have been registered successfully! Now, please, login.</strong>
</div>

<?php $this->load->view('partials/login_form'); ?>
<?php $this->load->view('partials/register_admin_form'); ?>
<?php $this->load->view('partials/register_user_form'); ?>



<!-- Footer -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 copy-container">
				<p class="copyright text-muted small">Copyright &copy; Expellibugus</p>
			</div>
		</div>
	</div>
</footer>



<script type='text/javascript' src="public/js/vendor/underscore.min.js"></script>
<script type='text/javascript' src="public/js/vendor/jquery-3.2.1.min.js"></script>
<script type='text/javascript' src="public/js/vendor/backbone.min.js"></script>
<script type='text/javascript' src="public/js/vendor/bootstrap.min.js"></script>
<script type='text/javascript' src="public/js/views/Landing.js"></script>
<script type='text/javascript' src="public/js/views/Login.js"></script>
<script type='text/javascript' src="public/js/views/Register.js"></script>
<script type='text/javascript' src="public/js/index.js"></script>
</body>
</html>
