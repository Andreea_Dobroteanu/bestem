<!DOCTYPE html>
<html>
<head>
  <title>Create Contact Form Using CodeIgniter</title>
  <link href='http://fonts.googleapis.com/css?family=Marcellus' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="container">
  <?php print $col1; ?>

  <div class="span4" id="error">
    <form name="register" method="post" action="register" id="register" class="register-form">
      <fieldset>
        <legend>Contact</legend>
        <label class="control-label" for="fname">First Name: </label>
        <input type="text" name="first_name" id="first_name" class="required">

        <label class="control-label" for="lname">Last Name: </label>
        <input type="text" name="first_name" id="last_name" class="required">

        <label class="control-label" for="email">E-mail: </label>
        <input type="text" name="email" id="email" class="required email">

        <label class="control-label" for="password">Password: </label>
        <input type="password" name="password" id="password" class="required">

        <label class="control-label" for="conf_password">Password: </label>
        <input type="password" name="conf_password" id="conf_password" class="required">

        <input type="submit" name="submit" id="submit" value="Submit" class="btn">
      </fieldset>
    </form>
</div>

  <div class="span4" id="error">
    <form name="login" method="post" action="login" id="login" class="login-form">
      <fieldset>
        <legend>Login</legend>
        <label class="control-label" for="lemail">First Name: </label>
        <input type="text" name="lemail" id="lemail" class="required">

        <label class="control-label" for="pass">Last Name: </label>
        <input type="password" name="pass" id="pass" class="required">

        <input type="submit" name="login_submit" id="login_submit" value="Login" class="btn">
      </fieldset>
    </form>
  </div>

  <script type='text/javascript' src="public/js/vendor/underscore.min.js"></script>
  <script type='text/javascript' src="public/js/vendor/jquery-3.2.1.min.js"></script>
  <script type='text/javascript' src="public/js/vendor/backbone.min.js"></script>
  <script type='text/javascript' src="public/js/register.js"></script>
</body>
</html>