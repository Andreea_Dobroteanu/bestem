<li>
    <a href="#" class="btn btn-default btn-lg edit-profile">
        <i class="fa fa-user fa-fw"></i>
        <span class="network-name">Edit Profile</span>
    </a>
</li>

<li>
    <a href="/<?php echo ($admin ? "jobs" : "applications") ?>" class="btn btn-default btn-lg your-jobs <?php echo ($admin ? "admin" : "") ?>">
        <i class="fa fa-file-text fa-fw"></i>
        <span class="network-name">Your jobs</span>
    </a>
</li>

<?php if ($admin == 0) { ?>
<li>
    <a href="/list_job_applied" class="btn btn-default btn-lg your-jobs">
        <i class="fa fa-file-text fa-fw"></i>
        <span class="network-name">Your applications</span>
    </a>
</li>
<?php } ?>