<li>
    <a href="#" id="user_login" data-toggle="modal" data-target="#login_modal" class="login btn btn-default btn-lg">
        <i class="fa fa-sign-in fa-fw"></i>
        <span class="network-name">Login</span>
    </a>
</li>

<li>
    <a href="#" id="user_register" data-toggle="modal"
       data-target="#user_register_modal" class="user register trigger btn btn-default btn-lg">
        <i class="fa fa-user fa-fw"></i>
        <span class="network-name">User Register</span>
    </a>
</li>
<li>
    <a href="#" id="user_register" data-toggle="modal"
       data-target="#admin_register_modal" class="admin register trigger btn btn-default btn-lg">
        <i class="fa fa-user fa-fw"></i>
        <span class="network-name">Admin Register</span>
    </a>
</li>