<div class="modal fade register-modal" id="admin_register_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <h1>Create new account</h1><br>
            <form class="register-form admin">
                <div class="form-group">
                    <label for="first-name" class="control-label required">First name:</label>
                    <input type="text" class="form-control first-name mandatory">
                </div>
                <div class="form-group">
                    <label for="first-name" class="control-label required">Last name:</label>
                    <input type="text" class="form-control last-name mandatory">
                </div>
                
                <div class="form-group">
                    <label for="register-email" class="control-label required">Email:</label>
                    <input type="text" class="form-control email mandatory">
                </div>
                
                <div class="form-group">
                    <label for="register-pass" class="control-label required">Password:</label>
                    <input type="password" class="form-control pass mandatory">
                </div>
                <div class="form-group">
                    <label for="register-pass" class="control-label required">Confirm password:</label>
                    <input type="password" class="form-control confirm-pass mandatory">
                </div>
                
                <input type="submit" name="register-admin" class="register registermodal-submit" value="Submit">
            </form>
        </div>
    </div>
</div>