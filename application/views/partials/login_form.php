<div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <h1>Login to Your Account</h1><br>
            <form class="login-form">
                <div class="form-group">
                    <label for="login-email" class="control-label">Email:</label>
                    <input type="text" class="form-control" id="login-email">
                </div>
                <div class="form-group">
                    <label for="login-pass" class="control-label">Password:</label>
                    <input type="password" class="form-control" id="login-pass">
                </div>

                <input type="submit" name="login" class="login loginmodal-submit" value="Login">
            </form>
        </div>
    </div>
</div>