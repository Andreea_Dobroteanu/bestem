<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>JobMatch</title>

    <!-- Bootstrap Core CSS -->
    <link href="public/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="public/css/landing-page.css" rel="stylesheet">
    <!-- Custom Fonts -->

    <link href="public/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
    <div class="container topnav">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand topnav" href="/">JobMatch</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <?php
                if ($admin < 0)
                    $this->load->view('partials/default_navbar_buttons');
                else
                    $this->load->view('partials/logged_nav_buttons');
                ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>


<div class="container user-jobs-container" id="user_jobs_container">
    <div class="row">
        <div class="col-lg-12">
            <h3>Jobs</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
            <input type="search" id="search" value="" class="form-control" placeholder="Search job...">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table" id="jobs_list_table">
                <thead>
                <tr>
                    <th>Company name</th>
                    <th>Candidate Profile</th>
                    <th>Job description</th>
                    <th>Recruiter Name</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $item) { ?>
                    <tr>
                        <td class="job-id"><?php echo $item->id_job ?></td>
                        <td class="comp-name"><?php echo $item->company_name ?></td>
                        <td class="cand-prof"><?php echo $item->candidate_profile ?></td>
                        <td class="job-descr"><?php echo $item->job_description ?></td>
                        <td class="recr-name"><?php echo $item->name_of_recruiter ?></td>

                        <td class="apply-job">
                            <a href="#" data-toggle="modal" data-target="#apply_job_modal" class="apply-job-button">
                                <i class="fa fa-sign-in"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <hr>
        </div>
    </div>
</div>

<a href="#" id="add_job_button" data-toggle="modal" data-target="#add_job_modal" class="add-job-button btn btn-default btn-lg">
    <i class="fa fa-plus"></i>
</a>

<div class="alert alert-success">
    <strong></strong>
</div>


<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="copyright copy-container text-muted small">Copyright &copy; Expellibugus</p>
            </div>
        </div>
    </div>
</footer>

<script type='text/javascript' src="public/js/vendor/underscore.min.js"></script>
<script type='text/javascript' src="public/js/vendor/jquery-3.2.1.min.js"></script>
<script type='text/javascript' src="public/js/vendor/backbone.min.js"></script>
<script type='text/javascript' src="public/js/vendor/bootstrap.min.js"></script>
<script src="//rawgithub.com/stidges/jquery-searchable/master/dist/jquery.searchable-1.0.0.min.js"></script>
<script type='text/javascript' src="public/js/views/Landing.js"></script>
<script type='text/javascript' src="public/js/views/Login.js"></script>
<script type='text/javascript' src="public/js/views/Register.js"></script>
<script type='text/javascript' src="public/js/views/AddJobForm.js"></script>
<script type='text/javascript' src="public/js/index.js"></script>
</body>
</html>
