(function () {
    var landing = new Landing();
    var login = new Login();

    if (location.pathname.match("/jobs") && location.pathname.match("/jobs").length) {
        var addJobForm = new AddJobForm();
        var editJobs = new EditJobsList();
        var deleteForm = new DeleteForm();
    }

    $jobsListTable = $('#jobs_list_table');
    if ($jobsListTable.length) {
        $jobsListTable.searchable({
            striped: true,
            searchType: 'fuzzy'
        });
    }

    var $apply_job_button = $(".apply-job-button");

    if($apply_job_button.length) {
        $apply_job_button.on("click", function(e) {
            var job_id = parseInt($(e.target).closest("tr").find(".job-id").eq(0).html(), 10);

            $.ajax({
                url: "jobs/apply",
                method: "POST",
                data: {
                    job_id: job_id
                },
                dataType: "json",
                complete: function(res) {
                    if (res.status === 200) {
                        $('.alert-success').text("Successfully applied to the selected job!");
                        $('.alert-success').fadeIn();

                        $(e.target).closest("tr").fadeOut();

                        setTimeout(function() {
                            $('.alert-success').fadeOut();
                        }, 3000);
                    }
                }
            });
        });
    }
})($);