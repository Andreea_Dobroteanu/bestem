/**
 * Created by Andreea on 4/22/2017.
 */

var Landing = Backbone.View.extend({
    el: "body",

    regAdminModalInit: false,
    regUserModalInit: false,

    events: {
        "click .login": "login",
        "click .logout": "logout",
        "click .register.trigger": "register"
    },

    initialize: function() {},

    register: function(e) {
        $el = $(e.target).hasClass("register") ? $(e.target) : $(e.target).closest('.register');

        if ($el.hasClass("admin")) {
            if (this.regAdminModalInit)
                return;

            this.regAdminModalInit = true;
        } else {
            if (this.regUserModalInit)
                return;

            this.regUserModalInit = true;
        }

        if ($el.hasClass('user')) {
            new Register('user');
        } else {
            new Register('admin');
        }
    },

    login: function() {
        $(document).trigger("user_login");
    },

    logout: function() {
        $.ajax({
            url: "home/logout",
            method: "POST",
            dataType: "json",
            success: function() {
                console.log ("Logout");
                location.href = "/";
            },
            error: function() {
                alert("Logout error");
            }
        });
    }
});

