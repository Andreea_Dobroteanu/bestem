var AddJobForm = Backbone.View.extend({
    el: '#add_job_modal',
    events: {
        "submit .add-job-form": "submitForm"
    },

    initialize: function() {},

    submitForm: function(e) {
        e.preventDefault();
        e.stopPropagation();

        var data = {
            company_name: this.$(".company-name").val(),
            candidate_profile: this.$(".candidate-profile").val(),
            name_of_recruiter: this.$(".name-of-recruiter").val(),
            job_description: this.$(".job-description").val(),
            id_domain: this.$("#add-job-domain").val()
        };

        $.ajax({
            url: "jobs/add_job",
            method: "POST",
            data: data,
            dataType: "json",
            success: _.bind(function(err) {
                $('.alert-success strong').text("Job added successfully!");

                // debugger;
                this.$el.modal('hide');
                setTimeout(function() {
                    $('.alert-success').fadeIn();
                    setTimeout(function() {
                        $('.alert-success').fadeOut();
                        location.reload();
                    }, 3000)
                }, 300);
            }, this),
            error: function() {
                console.log ("Error adding job :(");
            }
        });
    }
});