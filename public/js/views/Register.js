var Register = Backbone.View.extend({
    type: null,
    events: {
        "submit .register-form": "submitForm",
        "blur .form-control": "checkInput"
    },

    initialize: function(type) {
        this.type = type;
        this.setElement($('#' + type + '_register_modal'));
    },

    submitForm: function(e) {
        e.preventDefault();

        var base_elements = {
            first_name: this.$('.first-name'),
            last_name: this.$('.last-name'),
            email: this.$('.email'),
            pass: this.$('.pass'),
            confpass: this.$('.confirm-pass')
        };

        var errs = this.validateBaseElements(base_elements);

        if (!errs) {
            this.sendForm();
        }
    },

    sendForm: function(e) {
        var isAdmin = (this.type === "admin") ? 1 : 0;
        console.log(this.type);
        var data = {
            first_name: this.$('.first-name').val(),
            last_name: this.$('.last-name').val(),
            email: this.$('.email').val(),
            pass: this.$('.pass').val(),
            studies: "",
            job_domain: "",
            experience: "",
            admin: isAdmin
        };

        if (!isAdmin) {
            data["studies"] = this.$('.studies').val();
            data["job_domain"] = this.$('.domain').val();
            data["experience"] = this.$('.experience').val();
            console.log("User");
        }

        $.ajax({
            url: "register/register_user",
            method: "POST",
            data: data,
            dataType: "json",
            success: _.bind(function(err) {
                console.log ("Registered");
                if (!err.length) {
                    this.$el.modal('hide');
                    setTimeout(function() {
                        $('.register-success').fadeIn();
                        setTimeout(function() {
                            $('.register-success').fadeOut();
                        }, 2000)
                    }, 300);
                }
                console.log(err);
            }, this),
            error: function() {
                console.log ("Error registering");
            }
        });
    },

    validateBaseElements: function(data) {
        var errors = [];
        Object.keys(data).map(_.bind(function(field) {
            if (!this.checkInput(data[field])) {
                errors.push(field);
            }
        }, this));

        !this.checkPassMatch() ? errors.push("confpass") : null;

        return errors.length;
    },

    checkInput: function(e) {
        var $el;
        if (e.length) {
            $el = e;
        } else {
            $el = $(e.target);
        }

        /* Mandatory fields */
        if ($el.hasClass('mandatory')) {
            if (!$el.val().length) {
                $el.addClass('error');
                return false;
            } else {
                $el.removeClass('error');
            }
        }

        /* Email check */
        if ($el.hasClass('email')) {
            if (!this.validateEmail($el.val())) {
                $el.addClass('error');
                return false;
            } else {
                $el.removeClass('error');
            }
        }

        /* Password strength */
        if ($el.hasClass('pass')) {
            if (!this.checkPassStrength($el.val())) {
                $el.addClass('error');
                return false;
            } else {
                $el.removeClass('error');
            }
        }

        if ($el.hasClass('confirm-pass')) {
            return this.checkPassMatch();
        }

        return true;
    },

    validateEmail: function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },

    checkPassStrength: function(pass) {
        return pass.trim().length > 7;
    },

    checkPassMatch: function() {
        var pass = this.$('.pass').val();
        var confPass = this.$('.confirm-pass').val();

        if (pass !== confPass) {
            this.$('.pass').addClass('error');
            this.$('.confirm-pass').addClass('error');
            return false;
        } else {
            this.$('.pass').removeClass('error');
            this.$('.confirm-pass').removeClass('error');
        }
        return true;
    }
});