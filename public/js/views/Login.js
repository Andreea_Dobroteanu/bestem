var Login = Backbone.View.extend({
    el: '#login_modal',
    events: {
        "submit .login-form": "submitLogin"
    },

    initialize: function() {},

    submitLogin: function(e) {
        e.preventDefault();

        var email = $('#login-email').val();
        var pass = $('#login-pass').val();

        $.ajax({
            url: "login",
            method: "POST",
            data: {
                email: email,
                pass: pass

            },
            dataType: "json",
            success: function() {
                console.log ("Logged in");
                location.reload();
            },
            error: function(res, err) {
                if (res.status === 200) {
                    console.log ("Logged in");
                    location.reload();
                } else {
                    console.log ("Error logging in");
                    console.log(err);
                    var status = err['statusText'];

                }
            }
        });
    }
});